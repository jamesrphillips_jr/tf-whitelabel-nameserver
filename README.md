# tf-whitelabel-nameserver

Creates a white label dns server for hosting domains in AWS that are registered elsewhere.
This example creates A records and updates dns for white-label name servers.

https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/white-label-name-servers.html

## License

Apache License Version 2.0

<!-- BEGIN_TF_DOCS -->

## Example

```hcl
module "bibelobis" {
  source            = "git::https://gitlab.com/jamesrphillips_jr/tf-whitelabel-nameserver.git"
  delegation_set_id = "N00000000000000000D3"
  domain_name       = "domain.com"
}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.73.0 |
| <a name="provider_aws.us-east-1"></a> [aws.us-east-1](#provider\_aws.us-east-1) | 3.73.0 |
| <a name="provider_dns"></a> [dns](#provider\_dns) | 3.2.1 |

## Modules

No modules.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_delegation_set_id"></a> [delegation\_set\_id](#input\_delegation\_set\_id) | n/a | `string` | n/a | yes |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | n/a | `string` | n/a | yes |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_resource_policy.route53-query-logging-policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_resource_policy) | resource |
| [aws_kms_key.dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_route53_hosted_zone_dnssec.dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_hosted_zone_dnssec) | resource |
| [aws_route53_key_signing_key.key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_key_signing_key) | resource |
| [aws_route53_query_log.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_query_log) | resource |
| [aws_route53_record.a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.ns](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.soa](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_zone.zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_iam_policy_document.route53-query-logging-policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_route53_delegation_set.dset](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_delegation_set) | data source |
| [dns_a_record_set.nameservers](https://registry.terraform.io/providers/hashicorp/dns/latest/docs/data-sources/a_record_set) | data source |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_delegation_set_arn"></a> [delegation\_set\_arn](#output\_delegation\_set\_arn) | n/a |
| <a name="output_nameservers"></a> [nameservers](#output\_nameservers) | n/a |
<!-- END_TF_DOCS -->