#!/bin/sh
AWS=`which aws`

if [[ -x $AWS ]] && [[ -n $1 ]]
then
    $AWS route53 create-reusable-delegation-set --caller-reference $1
elif [[ -z $1 ]]
then
    echo "--caller-reference cannot be empty"
else 
    echo "aws cli not found or not in path"

fi