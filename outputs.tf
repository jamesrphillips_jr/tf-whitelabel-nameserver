output "delegation_set_arn" {
  value = data.aws_route53_delegation_set.dset.arn
}

output "nameservers" {
  value = aws_route53_record.a[*].fqdn
}
