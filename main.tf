#provider alias for cloudwatch log group
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}
# data sources
data "aws_route53_delegation_set" "dset" {
  id = var.delegation_set_id
}
data "dns_a_record_set" "nameservers" {
  count = length(data.aws_route53_delegation_set.dset.name_servers)
  host  = data.aws_route53_delegation_set.dset.name_servers[count.index]
}
data "aws_caller_identity" "current" {}
resource "aws_route53_zone" "zone" {
  name              = var.domain_name
  delegation_set_id = var.delegation_set_id
}
resource "aws_cloudwatch_log_group" "log_group" {
  provider          = aws.us-east-1
  name              = "/aws/route53/${aws_route53_zone.zone.name}"
  retention_in_days = 30
}

data "aws_iam_policy_document" "route53-query-logging-policy" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:log-group:/aws/route53/*"]

    principals {
      identifiers = ["route53.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_cloudwatch_log_resource_policy" "route53-query-logging-policy" {
  provider = aws.us-east-1

  policy_document = data.aws_iam_policy_document.route53-query-logging-policy.json
  policy_name     = "${var.domain_name}-route53-query-logging-policy"
}

resource "aws_route53_query_log" "this" {
  depends_on = [aws_cloudwatch_log_resource_policy.route53-query-logging-policy]

  cloudwatch_log_group_arn = aws_cloudwatch_log_group.log_group.arn
  zone_id                  = aws_route53_zone.zone.zone_id
}

resource "aws_kms_key" "dnssec" {
  customer_master_key_spec = "ECC_NIST_P256"
  deletion_window_in_days  = 7
  key_usage                = "SIGN_VERIFY"
  policy = jsonencode({
    Statement = [
      {
        Action = [
          "kms:DescribeKey",
          "kms:GetPublicKey",
          "kms:Sign",
        ],
        Effect = "Allow"
        Principal = {
          Service = "dnssec-route53.amazonaws.com"
        }
        Sid      = "Allow Route 53 DNSSEC Service",
        Resource = "*"
      },
      {
        Action = "kms:CreateGrant",
        Effect = "Allow"
        Principal = {
          Service = "dnssec-route53.amazonaws.com"
        }
        Sid      = "Allow Route 53 DNSSEC Service to CreateGrant",
        Resource = "*"
        Condition = {
          Bool = {
            "kms:GrantIsForAWSResource" = "true"
          }
        }
      },
      {
        Action = "kms:*"
        Effect = "Allow"
        Principal = {
          AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        }
        Resource = "*"
        Sid      = "AllowKeyAdministration"
      },
    ]
    Version = "2012-10-17"
  })
}

resource "aws_route53_key_signing_key" "key" {
  hosted_zone_id             = aws_route53_zone.zone.id
  key_management_service_arn = aws_kms_key.dnssec.arn
  name                       = "${var.domain_name}-dnssec"
}

resource "aws_route53_hosted_zone_dnssec" "dnssec" {
  depends_on = [
    aws_route53_key_signing_key.key
  ]
  hosted_zone_id = aws_route53_key_signing_key.key.hosted_zone_id
}

resource "aws_route53_record" "a" {
  count   = length(data.aws_route53_delegation_set.dset.name_servers)
  zone_id = aws_route53_zone.zone.zone_id
  name    = "ns${count.index}.${var.domain_name}"
  type    = "A"
  ttl     = "60"
  records = data.dns_a_record_set.nameservers[count.index].addrs
}

resource "aws_route53_record" "ns" {
  allow_overwrite = true
  zone_id         = aws_route53_zone.zone.zone_id
  name            = var.domain_name
  type            = "NS"
  ttl             = "60" # set to 60, then set back to 172800
  records = [
    format("%s%s", aws_route53_record.a[0].fqdn, "."),
    format("%s%s", aws_route53_record.a[1].fqdn, "."),
    format("%s%s", aws_route53_record.a[2].fqdn, "."),
    format("%s%s", aws_route53_record.a[3].fqdn, "."),
  ]
}

resource "aws_route53_record" "soa" {
  allow_overwrite = true
  zone_id         = aws_route53_zone.zone.zone_id
  name            = var.domain_name
  type            = "SOA"
  ttl             = "60" #set to 60 then change back to 900
  records = [
    format("%s. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 60", aws_route53_record.a[1].fqdn)
    # format("%s. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400", aws_route53_record.a[1].fqdn)
  ]
}
