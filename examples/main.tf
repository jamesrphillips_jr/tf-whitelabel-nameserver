module "bibelobis" {
  source            = "git::https://gitlab.com/jamesrphillips_jr/tf-whitelabel-nameserver.git"
  delegation_set_id = "N00000000000000000D3"
  domain_name       = "domain.com"
}